package main

import (
	"crypto/tls"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var clientId = `5d51f771-b86a-419e-ad25-27696eafc02c`
var redirectUrl = `https://localhost/adfs/oauth2/authorize?response_type=code&client_id=5d51f771-b86a-419e-ad25-27696eafc02c&redirect_uri=https://localhost:444/oauth`
var tokenRedirectUrl = `https://localhost/adfs/oauth2/token?response_type=code&client_id=5d51f771-b86a-419e-ad25-27696eafc02c&redirect_uri=https://localhost:444/oauth&code=`
var tokenRedirectHost = `https://localhost:444/oauth`

func main() {
	http.HandleFunc("/login", handleRedirect) //This is the exact url I want to redirect from
	http.HandleFunc("/oauth", handleAdfsResponse)
	http.ListenAndServeTLS(":444", "cert.pem", "key.pem", nil)
}

func handleAdfsResponse(w http.ResponseWriter, r *http.Request) {
	//_ = r.ParseForm()
	fmt.Printf("Handling adfs response")
	state, ok := r.URL.Query()["state"]
	if !ok {
		log.Printf("No url parameter `state` \n")
	} else {
		log.Printf("State is: %s", state[0])
	}

	if state[0] == "12345" {
		code, ok := r.URL.Query()["code"]
		if !ok {
			fmt.Printf("Error! NO CODE!")
		} else {
			token, _ := sendTokenRequest(code[0])
			if token != "" {
				fmt.Printf("Token is %s\n", token)
				http.Redirect(w, r, "https://www.yandex.ru", http.StatusSeeOther)
			} else {
				fmt.Printf("CANNOT OBTAIN TOKEN!")
			}
		}

	} else {
		tmpl, _ := template.ParseFiles("./templates/oauth.html")
		tmpl.Execute(w, state[0])
	}

	tmpl, _ := template.ParseFiles("./templates/oauth.html")
	tmpl.Execute(w, state[0])
}

func handleRedirect(rw http.ResponseWriter, req *http.Request) {
	fmt.Printf("Handling login...redirecting with state 12345")
	http.Redirect(rw, req, redirectUrl+"&state=12345", http.StatusSeeOther)
}

func sendTokenRequest(code string) (string, error) {
	fmt.Printf("Making token request, Code is: %s\n", code)

	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	data.Set("client_id", clientId)
	data.Set("code", code)
	data.Set("redirect_uri", tokenRedirectHost)

	client := &http.Client{Transport: &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	},
	}
	r, _ := http.NewRequest("POST", tokenRedirectUrl+code, strings.NewReader(data.Encode()))
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Content-Length", strconv.Itoa(len(data.Encode())))

	resp, _ := client.Do(r)

	log.Printf(resp.Status)

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error when getting response from ADFS /token endpoint")
		log.Fatal(err)
	}

	for k,v := range resp.Header{
		log.Printf("Header[%q] = %q\n", k, v)
	}
	respBody := string(bodyBytes)
	log.Printf("Token response body: %s", respBody)

	return string(respBody), nil
}
