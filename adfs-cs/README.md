# ADFSAuthSample

Authentication sample with retreiving username from AD FS 2019 (Windows server 2019)
- You need working AD FS server to use this sample
- You need Relaying Party Trust configured as in M$ docs.


Link to AD FS Configuration: https://docs.microsoft.com/ru-ru/windows-server/identity/ad-fs/operations/create-a-relying-party-trust