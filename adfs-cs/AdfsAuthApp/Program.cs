﻿using System;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Security;



namespace AdfsAuthApp
{
    public class ADFSUsernameMixedTokenProvider
    {
        private readonly Uri adfsUserNameMixedEndpoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="ADFSUsernameMixedTokenProvider"/> class
        /// </summary>
        /// <param name="adfsUserNameMixedEndpoint">i.e. https://adfs.mycompany.com/adfs/services/trust/13/usernamemixed </param>
        public ADFSUsernameMixedTokenProvider(Uri adfsUserNameMixedEndpoint)
        {
            this.adfsUserNameMixedEndpoint = adfsUserNameMixedEndpoint;
        }

        /// <summary>
        /// Requests a security token from the ADFS server
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="password">The password</param>
        /// <param name="endpoint">The ADFS endpoint</param>
        /// <returns></returns>
        public GenericXmlSecurityToken RequestToken(string username, string password, string endpoint)
        {
            var binding = new WS2007HttpBinding(SecurityMode.TransportWithMessageCredential);
            binding.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
            binding.Security.Message.EstablishSecurityContext = false;
            WSTrustChannelFactory factory = new WSTrustChannelFactory(
                binding,
                new EndpointAddress(adfsUserNameMixedEndpoint));
            
            factory.TrustVersion = TrustVersion.WSTrust13;
            factory.Credentials.UserName.UserName = username;
            factory.Credentials.UserName.Password = password;

            RequestSecurityToken token = new RequestSecurityToken
            {
                RequestType = RequestTypes.Issue,
                AppliesTo = new EndpointReference(endpoint),
                KeyType = KeyTypes.Bearer
            };

            IWSTrustChannelContract channel = factory.CreateChannel();

            return channel.Issue(token) as GenericXmlSecurityToken;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
            
            var tokenProvider = new ADFSUsernameMixedTokenProvider(new Uri("https://adfs.compile.name/adfs/services/trust/13/usernamemixed"));
            try
            {
                var username = "user@domain.com";
                var password = "password";
                var endpoint = "https://sample-site.com/auth"; // As configured at AD FS server 
                var t = tokenProvider.RequestToken(username, password, endpoint); //TODO: take token XML and find credentials values (username, email, etc)
                
                Console.WriteLine(t.TokenXml.OuterXml);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
            Console.ReadKey();
        }
    }
}
