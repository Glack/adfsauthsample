package main

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
	"github.com/dgrijalva/jwt-go"
)

type AuthConfig struct {
	ServerPort string `json: serverPort`
	Api string `json:"api"`
	Endpoint string `json:"endpoint""`
	ApiClientId string `json:"clientId"`
	AuthPath string `json:"authPath"`
	TokenPath string `json:"tokenPath"`
	AuthRedirectUri string `json:"authRedirectUri"`
	TokenRedirectUri string `json:tokenRedirectUri`
}

var config AuthConfig

func main(){
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	file, err := os.OpenFile("AuthSample.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Cant open log file")
	}
	defer file.Close()
	mw := io.MultiWriter(file, os.Stdout)
	log.SetOutput(mw)

	log.Printf("Starting server")
	log.Printf("Try to read config\n")

	bytes, err := ioutil.ReadFile("auth.conf")
	if err != nil {
		log.Fatalf("Error when reading file auth.conf\n$v\n", err)
	}

	log.Printf("%s\n",string(bytes))
	err = json.Unmarshal(bytes, &config)
	if err != nil {
		log.Fatalf("Error when unmarshalling data from config file\n$v\n", err)
	}
	log.Printf("Readed config: %v\n", config)

	//http.HandleFunc("/"+strings.TrimLeft(config.AuthRedirectUri, "/"), ServeAuth)
	http.HandleFunc(config.Endpoint, ServeAuth)
	log.Printf("Listening on: %s\n", "localhost:"+config.ServerPort)
	err = http.ListenAndServeTLS("localhost:"+config.ServerPort, "cert.pem", "key.pem", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

	log.Printf("Terminated\n")
}

func ServeAuth(w http.ResponseWriter, r *http.Request) {
	var authCode string
	var tokenStr string
	var err error
	log.Printf("Got a request")
	log.Printf("Method: %s", r.Method)

	authCode, err = ObtainAuthCode(r)
	if err != nil {
		log.Printf("No auth code\n$v\n", err)
	}
	log.Printf("authorization code is : %v", authCode)

	state, err := ObtainState(r)
	if err != nil {
		log.Printf("ERR: No state!")
	}
	log.Printf("state is :%s\n", state)

	log.Printf("Send request to obtain JWT Token")

	tokenStr, err = sendTokenRequest(authCode)

	var dat map[string]interface{}

	if err := json.Unmarshal([]byte(tokenStr), &dat); err != nil {
		panic(err)
	}
	fmt.Println(dat)

	idToken := dat["id_token"].(string)
	token, _, err := new(jwt.Parser).ParseUnverified(idToken, jwt.MapClaims{})
	if err != nil {
		fmt.Println(err)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		for k, v := range claims{
			log.Printf("Claim <%s> == %s", k, v)
		}

	} else {
		log.Println(err)
	}
	//makeTokenRequest(authCode)

	//if r.Method == "GET" {
	//
	//}
	//log.Printf(token)
}

func ObtainState(r *http.Request) (string, error) {
	log.Printf("Obtaining 'state' parameter from AD FS redirect\n")
	state := r.URL.Query()["state"]
	if state != nil {
		return state[0], nil
	} else {
		return "", errors.New("No state parsed")
	}
}

func sendTokenRequest(code string) (string, error) {
	fmt.Printf("Making token request, Code is: %s\n", code)

	resource := "adfs/oauth2/token"
	//resource := config.TokenPath
	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	//data.Set("client_id", config.ApiClientId)
	data.Set("client_id", config.ApiClientId)
	data.Set("code", code)
	//data.Set("redirect_uri", config.TokenRedirectUri)
	data.Set("redirect_uri", config.TokenRedirectUri)

	//u, _ := url.ParseRequestURI(config.Api)
	u, _ := url.ParseRequestURI(config.Api)
	u.Path = resource
	urlStr := u.String() // "https://api.com/user/"

	client := &http.Client{Transport: &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		TLSClientConfig: &tls.Config{
			// See comment above.
			// UNSAFE!
			// DON'T USE IN PRODUCTION!
			InsecureSkipVerify: true,
		},
	},
	}
	r, _ := http.NewRequest("POST", urlStr, strings.NewReader(data.Encode()))
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Content-Length", strconv.Itoa(len(data.Encode())))

	resp, _ := client.Do(r)

	log.Printf(resp.Status)

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error when getting response from ADFS /token endpoint")
		log.Fatal(err)
	}

	for k,v := range resp.Header{
		log.Printf("Header[%q] = %q\n", k, v)
	}
	respBody := string(bodyBytes)
	log.Printf("Token response body: %s", respBody)

	return string(respBody), nil
}

func ObtainAuthCode(r *http.Request) (string, error) {
	log.Printf("Obtaining 'code' parameter from AD FS redirect\n")
	code := r.URL.Query()["code"]
	if code != nil {
		return code[0], nil
	} else {
		return "", errors.New("No code parsed")
	}
	//query, err := url.ParseQuery(r.URL.RawQuery)
	//if err != nil {
	//	log.Printf("Error at parsing url query")
	//}
	//
	//log.Printf("Got a request with parameters:\n")
	//for k, v := range query{
	//	log.Printf("name: %v, value: %v", k, v[0])
	//}
	//if query["code"] != nil {
	//	return query["code"][0], nil
	//} else {
	//	return "", errors.New("there is no 'code' parameter in request")
	//}
}


